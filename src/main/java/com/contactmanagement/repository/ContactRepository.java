package com.contactmanagement.repository;

import com.contactmanagement.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    @Query(value = "SELECT * FROM contact u WHERE CONCAT(u.first_name, ' ', u.email,' ',u.last_name) LIKE %?1%", nativeQuery = true)
    List<Contact> findAllBySearchValue(String search);

}
