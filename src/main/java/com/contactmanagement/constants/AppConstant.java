package com.contactmanagement.constants;

public class AppConstant {
    public interface StatusCodes {
        int SUCCESS = 1;
        int FAILURE = 0;
        int SESSION_EXPIRE = 2;
    }
    public interface ErrorTypes {
        String CONTACT_DOES_NOT_EXISTS = "Contact Doesn't Exists";
    }
    public interface ErrorCodes {
        String CONTACT_DOES_NOT_EXISTS_CODE = "175";
    }
    public interface ErrorMessages {
        String CONTACT_DOES_NOT_EXISTS_MESSAGE = "Contact Doesn't Exists";
    }

}
