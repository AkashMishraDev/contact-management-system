package com.contactmanagement.constants;

public interface RestMappingConstants {
    public interface Contact {

        String CREATE_CONTACT = "/createcontact";

        String LIST_OF_CONTACT = "/contactlist";

        String EDIT_CONTACT = "/editcontact";

        String DELETE_CONTACT = "/deletecontact/{id}";

        String GET_CONTACT_DETAILS = "/getcontactdetails/{id}";
    }
}