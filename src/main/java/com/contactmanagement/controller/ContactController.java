package com.contactmanagement.controller;

import com.contactmanagement.constants.RestMappingConstants;
import com.contactmanagement.request.ContactRequest;
import com.contactmanagement.request.EditContactRequest;
import com.contactmanagement.response.BaseApiResponse;
import com.contactmanagement.response.ResponseBuilder;
import com.contactmanagement.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ContactController {
    @Autowired
    ContactService contactService;

    @PostMapping(path = RestMappingConstants.Contact.CREATE_CONTACT)
    public ResponseEntity<BaseApiResponse> createContact(@Valid @RequestBody ContactRequest requestModel) {
        BaseApiResponse baseApiResponse = ResponseBuilder
                .getSuccessResponse(contactService.createContact(requestModel));
        return new ResponseEntity<BaseApiResponse>(baseApiResponse, HttpStatus.OK);
    }

    @GetMapping(path = RestMappingConstants.Contact.LIST_OF_CONTACT)
    public ResponseEntity<BaseApiResponse> getListOfContact(@RequestParam(required = false) String search) {
        BaseApiResponse baseApiResponse = ResponseBuilder.getSuccessResponse(contactService.getContactResponse(search));
        return new ResponseEntity<BaseApiResponse>(baseApiResponse, HttpStatus.OK);
    }

    @PutMapping(path = RestMappingConstants.Contact.EDIT_CONTACT)
    public ResponseEntity<BaseApiResponse> editContact(@Valid @RequestBody EditContactRequest requestModel) {
        BaseApiResponse baseApiResponse = ResponseBuilder
                .getSuccessResponse(contactService.editContact(requestModel));
        return new ResponseEntity<BaseApiResponse>(baseApiResponse, HttpStatus.OK);
    }

    @DeleteMapping(path = RestMappingConstants.Contact.DELETE_CONTACT)
    public ResponseEntity<BaseApiResponse> deleteContact(@PathVariable Long id) {
        BaseApiResponse baseApiResponse = ResponseBuilder
                .getSuccessResponse(contactService.deleteContact(id));
        return new ResponseEntity<BaseApiResponse>(baseApiResponse, HttpStatus.OK);
    }

    @GetMapping(path = RestMappingConstants.Contact.GET_CONTACT_DETAILS)
    public ResponseEntity<BaseApiResponse> getContactDetails(@PathVariable Long id) {
        BaseApiResponse baseApiResponse = ResponseBuilder
                .getSuccessResponse(contactService.getContactDetails(id));
        return new ResponseEntity<BaseApiResponse>(baseApiResponse, HttpStatus.OK);
    }
}
