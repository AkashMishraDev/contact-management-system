package com.contactmanagement.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = {"cause","stackTrace","suppressed","localizedMessage"})
public class InvalidInputException extends AppException{

    public InvalidInputException(String errorType, String errorCode, String message) {
        super(errorType, errorCode, message);
    }


}
