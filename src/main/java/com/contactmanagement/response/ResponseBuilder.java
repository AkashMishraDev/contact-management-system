package com.contactmanagement.response;

import com.contactmanagement.constants.AppConstant;
import com.contactmanagement.exception.AppException;

public class ResponseBuilder {
    public static BaseApiResponse getSuccessResponse(Object responseData) throws AppException {

        BaseApiResponse baseApiResponse = new BaseApiResponse();
        baseApiResponse.setResponseStatus(new ResponseStatus(AppConstant.StatusCodes.SUCCESS));
        baseApiResponse.setResponseData(responseData);

        return baseApiResponse;
    }

    public static BaseApiResponse getSuccessResponse() throws AppException {

        BaseApiResponse baseApiResponse = new BaseApiResponse();
        baseApiResponse.setResponseStatus(new ResponseStatus(AppConstant.StatusCodes.SUCCESS));
        baseApiResponse.setResponseData(null);

        return baseApiResponse;
    }
}
