package com.contactmanagement.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ContactResponse {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
}
