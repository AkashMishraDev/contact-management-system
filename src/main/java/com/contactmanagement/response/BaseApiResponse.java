package com.contactmanagement.response;

public class BaseApiResponse {

    private ResponseStatus responseStatus;
    private Object responseData;



    public BaseApiResponse(Object responseData) {

        this.responseData = responseData;
    }

    public BaseApiResponse() {

        this.responseData = null;
    }

    public BaseApiResponse(boolean error, ResponseStatus responseStatus, Object responseData, String message) {
        super();

        this.responseStatus = responseStatus;
        this.responseData = responseData;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }

    public void setStatusCode(int success) {
        // TODO Auto-generated method stub

    }


}
