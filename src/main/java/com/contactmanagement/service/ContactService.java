package com.contactmanagement.service;

import com.contactmanagement.request.ContactRequest;
import com.contactmanagement.request.EditContactRequest;
import com.contactmanagement.response.ContactResponse;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ContactService {
    public String createContact(ContactRequest sampleRequest);
    List<ContactResponse> getContactResponse(String search);
    public String editContact(EditContactRequest editRequest);
    public String deleteContact(Long id);
    public ContactResponse getContactDetails(Long id);
}
