package com.contactmanagement.serviceImpl;

import com.contactmanagement.constants.AppConstant;
import com.contactmanagement.entity.Contact;
import com.contactmanagement.exception.InvalidInputException;
import com.contactmanagement.repository.ContactRepository;
import com.contactmanagement.request.ContactRequest;
import com.contactmanagement.request.EditContactRequest;
import com.contactmanagement.response.ContactResponse;
import com.contactmanagement.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;
import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    ContactRepository contactRepository;


    @Override
    public String createContact(ContactRequest contactRequest) {
        Contact contact = Contact.builder()
                .firstName(contactRequest.getFirstName())
                .lastName(contactRequest.getLastName())
                .email(contactRequest.getEmail())
                .phone(contactRequest.getPhone())
                .build();
        contactRepository.save(contact);
        return "contact created successfully";
    }

    @Override
    public List<ContactResponse> getContactResponse(String search) {
        if(search!=null) {
            return contactRepository.findAllBySearchValue(search).stream().map(this:: convertToModel).collect(Collectors.toList());
        }
        else{
            return contactRepository.findAll().stream().map(this:: convertToModel).collect(Collectors.toList());
        }
    }
    private ContactResponse convertToModel(Contact contact) {
        return ContactResponse.builder().firstName(contact.getFirstName())
                .lastName(contact.getLastName())
                .email(contact.getEmail())
                .phone(contact.getPhone()).build();
    }

    @Override
    public String editContact(EditContactRequest editRequest) {
        Contact contact =contactRepository.findById(editRequest.getId()).orElseThrow(() -> new InvalidInputException(AppConstant.ErrorTypes.CONTACT_DOES_NOT_EXISTS,
                AppConstant.ErrorCodes.CONTACT_DOES_NOT_EXISTS_CODE,
                AppConstant.ErrorMessages.CONTACT_DOES_NOT_EXISTS_MESSAGE)
        );
        contact.setFirstName(editRequest.getFirstName());
        contact.setLastName(editRequest.getLastName());
        contact.setEmail(editRequest.getEmail());
        contact.setPhone(editRequest.getPhone());
        contactRepository.save(contact);
        return "contact update successfully";
    }

    @Override
    public String deleteContact(Long id) {
        Contact contact =contactRepository.findById(id)
                .orElseThrow(() -> new InvalidInputException(AppConstant.ErrorTypes.CONTACT_DOES_NOT_EXISTS,
                        AppConstant.ErrorCodes.CONTACT_DOES_NOT_EXISTS_CODE,
                        AppConstant.ErrorMessages.CONTACT_DOES_NOT_EXISTS_MESSAGE)
        );
        contactRepository.delete(contact);
        return "contact deleted successfully";
    }

    @Override
    public ContactResponse getContactDetails(Long id) {
        Contact contact =contactRepository.findById(id).orElseThrow(() -> new InvalidInputException(AppConstant.ErrorTypes.CONTACT_DOES_NOT_EXISTS,
                AppConstant.ErrorCodes.CONTACT_DOES_NOT_EXISTS_CODE,
                AppConstant.ErrorMessages.CONTACT_DOES_NOT_EXISTS_MESSAGE)
        );
        ContactResponse contactResponse = new ContactResponse();
        contactResponse.setFirstName(contact.getFirstName());
        contactResponse.setLastName(contact.getLastName());
        contactResponse.setPhone(contact.getPhone());
        contactResponse.setEmail(contact.getEmail());
        return contactResponse;
    }
}
