package com.contactmanagement.advicecontroller;

import com.contactmanagement.constants.AppConstant;
import com.contactmanagement.exception.InvalidInputException;
import com.contactmanagement.response.BaseApiResponse;
import com.contactmanagement.response.ResponseStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class UniversalControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<BaseApiResponse> invalidContactException(InvalidInputException invalidInputException, HttpServletRequest request) {
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        baseApiResponse.setResponseStatus(new ResponseStatus(AppConstant.StatusCodes.FAILURE));
        baseApiResponse.setResponseData(invalidInputException);
        return new ResponseEntity<>(baseApiResponse, HttpStatus.OK);
    }
}
