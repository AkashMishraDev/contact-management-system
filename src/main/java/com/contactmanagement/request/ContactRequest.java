package com.contactmanagement.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactRequest {

    @Pattern(regexp = "[a-zA-Z]+\\.?", message = "Please Enter Valid first name")
    private String firstName;
    @Pattern(regexp = "[a-zA-Z]+\\.?", message = "Please Enter Valid last name")
    private String lastName;
    @Pattern(regexp = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$", message = "Please Enter Valid Email")
    private String email;
    private String phone;
}
